﻿using UnityEngine;
using System.Collections;

public class DisplayFPS : MonoBehaviour
{
	private GUIText text;
	private float fcount;
	private float ftime;
	private float fps;

	private float deltaTime = 0f;
	private float lastTime = 0f;

	// Use this for initialization
	void Start ()
	{
		text = gameObject.GetComponent<GUIText>();
		fcount = 0;
		ftime = 0;
		lastTime = Time.realtimeSinceStartup;
		//QualitySettings.vSyncCount = 0;
		//Application.targetFrameRate = 10000;
	}
	
	// Update is called once per frame
	void Update ()
	{
		deltaTime = Time.realtimeSinceStartup - lastTime;
		lastTime = Time.realtimeSinceStartup;

		fcount += 1;
		ftime += deltaTime;

		if (ftime >= 0.5)
		{
			fps = fcount / ftime;
			fcount = 0;
			ftime = 0;
		}
		text.text = "FPS_ " + fps.ToString("0.00000");
	}
}
