﻿using UnityEngine;
using System.Collections;

public class GUI_Test : MonoBehaviour
{

	public GUIText text;

	private Box_Model BM;

	// Use this for initialization
	void Start ()
	{
		BM = gameObject.GetComponent<Box_Model>();
		BM.Register(mOnMouseDown);
		//BM.Register(mOnMouseHover);
		//BM.Unregister(mOnMouseDown);
	}

	void mOnMouseDown ( Box_Event.OnMouseDown e )
	{
		text.text ="";
		if (e.GetPressed(Box_Event.MouseButtonLeft))
			text.text = "Left !";
		if (e.GetPressed(Box_Event.MouseButtonRight))
			text.text += "Right !";
		if (e.GetPressed(Box_Event.MouseButtonMiddle))
			text.text += "Middle !";
	}

	void mOnMouseHover(Box_Event.OnMouseHover e)
	{
		text.text=e.position.ToString();
	}
}
