﻿//#define TESTPLAY
using UnityEngine;
//using UnityEditor;
using System.Collections;

#if (UNITY_EDITOR)
[ExecuteInEditMode]
#endif
public class Box_Controller : MonoBehaviour
{
	private bool[] mouseDown_track = {false,false,false};
	private bool[] mouseUp_track = {false,false,false};
	private float deltaTime = 0f;
	private float lastTime = 0f; 
	private static Vector2 MousePosition;

	public static Vector2 GetMousePosition ()
	{
		return MousePosition;
	}


#if (UNITY_EDITOR && !TESTPLAY)
	void OnGUI ()
#else
	void Update ()
#endif
	{
		float t = Time.realtimeSinceStartup;
		if (t - lastTime < 1f)
		{
			deltaTime = t - lastTime;
		}
		lastTime = t;


#if (UNITY_EDITOR && !TESTPLAY)
		MousePosition = Event.current.mousePosition;
		MousePosition.y = Screen.height - MousePosition.y;
#else
		MousePosition = Input.mousePosition;
#endif

		Box_Model[] Boxes = GameObject.FindObjectsOfType(typeof(Box_Model)) as Box_Model[];

		Box myBox;
		int MouseButton = 0;
#if (!UNITY_EDITOR || TESTPLAY)

		bool[] isMouseDown = new bool[3];
		bool[] isMouseButton = new bool[3];
		bool[] isMouseUp = new bool[3];
		isMouseButton[0] = Input.GetMouseButton(0);
		isMouseButton[1] = Input.GetMouseButton(1);
		isMouseButton[2] = Input.GetMouseButton(2);

		isMouseDown[0] = ( isMouseButton[0] && !mouseDown_track[0] );
		isMouseDown[1] = ( isMouseButton[1] && !mouseDown_track[1] );
		isMouseDown[2] = ( isMouseButton[2] && !mouseDown_track[2] );

		isMouseUp[0] = ( !isMouseButton[0] && !mouseUp_track[0] );
		isMouseUp[1] = ( !isMouseButton[1] && !mouseUp_track[1] );
		isMouseUp[2] = ( !isMouseButton[2] && !mouseUp_track[2] );

		if (isMouseButton[0])
			MouseButton |= Box_Event.MouseButtonLeft;
		if (isMouseButton[1])
			MouseButton |= Box_Event.MouseButtonRight;
		if (isMouseButton[2])
			MouseButton |= Box_Event.MouseButtonMiddle;

#else
		if (Event.current.button == 0)
			MouseButton |= Box_Event.MouseButtonLeft;
		if (Event.current.button == 1)
			MouseButton |= Box_Event.MouseButtonRight;
		if (Event.current.button == 2)
			MouseButton |= Box_Event.MouseButtonMiddle;
#endif

		bool[] mouseDown_selected = {false,false,false};
		bool[] mouseUp_selected = {false,false,false};
#if (UNITY_EDITOR && !TESTPLAY)
		bool MouseInGameArea = false;
		GameObject active = UnityEditor.Selection.activeGameObject;
#else
		GameObject active = null;
#endif
		Box_Model activeBM = null;
		foreach (Box_Model box in Boxes)
		{
			if (box == null)
				continue;
			myBox = box.GetBox;
			if (myBox == null)
				continue;
			else
			{
				box.myEvents.mouseDownTimer[0] += deltaTime;
				box.myEvents.mouseDownTimer[1] += deltaTime;
				box.myEvents.mouseDownTimer[2] += deltaTime;
				box.myEvents.mouseUpTimer[0] += deltaTime;
				box.myEvents.mouseUpTimer[1] += deltaTime;
				box.myEvents.mouseUpTimer[2] += deltaTime;

				if (Box_Page.FocusedPage != null)
					if (Box_Page.FocusedPage.isModal)
						if (box.myPage != Box_Page.FocusedPage)
							continue;//not part of the focused modal page, ignore events.

				if (box.myPage != null)
					if (box.myPage.isLocked || !box.myPage.isVisible)
						continue; //page is locked or not visible, ignore events.

				Box_Rect myBoxPos = myBox.GetPosition();
				if (( MousePosition.x >= myBoxPos.x.InPixels() ) && ( MousePosition.x <= myBoxPos.x.InPixels() + myBoxPos.width.InPixels() ) && ( MousePosition.y >= myBoxPos.y.InPixels() ) && ( MousePosition.y <= myBoxPos.y.InPixels() + myBoxPos.height.InPixels() ))
				{

#if (UNITY_EDITOR && !TESTPLAY)
					MouseInGameArea = true;

					if (( Event.current.type == EventType.mouseDown ) && ( !mouseDown_track[0] ) && ( box.property.Selectable ))
#else
					if (( isMouseDown[0] ) && ( !mouseDown_track[0] ) && ( box.property.Selectable ))
#endif
					{
						if (mouseDown_selected[0])
						{
							if (box.gameObject.transform.localPosition.z >= active.transform.localPosition.z)
							{
								activeBM = box;
								active = box.gameObject;
							}
						}
						else
						{
							mouseDown_selected[0] = true;
							activeBM = box;
							active = box.gameObject;
						}
					}

#if (UNITY_EDITOR && !TESTPLAY)
					if (( Event.current.type == EventType.mouseDown ) && ( !mouseDown_track[1] ) && ( box.property.Selectable ))
#else
					if (( isMouseDown[1] ) && ( !mouseDown_track[1] ) && ( box.property.Selectable ))
#endif
					{
						if (mouseDown_selected[1])
						{
							if (box.gameObject.transform.localPosition.z >= active.transform.localPosition.z)
							{
								activeBM = box;
								active = box.gameObject;
							}
						}
						else
						{
							mouseDown_selected[1] = true;
							activeBM = box;
							active = box.gameObject;
						}
					}

#if (UNITY_EDITOR && !TESTPLAY)
					if (( Event.current.type == EventType.mouseDown ) && ( !mouseDown_track[2] ) && ( box.property.Selectable ))
#else
					if (( isMouseDown[2] ) && ( !mouseDown_track[2] ) && ( box.property.Selectable ))
#endif
					{
						if (mouseDown_selected[2])
						{
							if (box.gameObject.transform.localPosition.z >= active.transform.localPosition.z)
							{
								activeBM = box;
								active = box.gameObject;
							}
						}
						else
						{
							mouseDown_selected[2] = true;
							activeBM = box;
							active = box.gameObject;
						}
					}

#if (UNITY_EDITOR && !TESTPLAY)
					if (( Event.current.type == EventType.mouseUp ) && ( !mouseUp_track[0] ) && ( box.property.Selectable ))
#else
					if (( isMouseUp[0] ) && ( !mouseUp_track[0] ) && ( box.property.Selectable ))
#endif
					{
						if (mouseUp_selected[0])
						{
							if (box.gameObject.transform.localPosition.z >= active.transform.localPosition.z)
							{
								activeBM = box;
								active = box.gameObject;
							}
						}
						else
						{
							mouseUp_selected[0] = true;
							activeBM = box;
							active = box.gameObject;
						}
					}

#if (UNITY_EDITOR && !TESTPLAY)
					if (( Event.current.type == EventType.mouseUp ) && ( !mouseUp_track[1] ) && ( box.property.Selectable ))
#else
					if (( isMouseUp[1] ) && ( !mouseUp_track[1] ) && ( box.property.Selectable ))
#endif
					{
						if (mouseUp_selected[1])
						{
							if (box.gameObject.transform.localPosition.z >= active.transform.localPosition.z)
							{
								activeBM = box;
								active = box.gameObject;
							}
						}
						else
						{
							mouseUp_selected[1] = true;
							activeBM = box;
							active = box.gameObject;
						}
					}

#if (UNITY_EDITOR && !TESTPLAY)
					if (( Event.current.type == EventType.mouseUp ) && ( !mouseUp_track[1] ) && ( box.property.Selectable ))
#else
					if (( isMouseUp[1] ) && ( !mouseUp_track[1] ) && ( box.property.Selectable ))
#endif
					{
						if (mouseUp_selected[1])
						{
							if (box.gameObject.transform.localPosition.z >= active.transform.localPosition.z)
							{
								activeBM = box;
								active = box.gameObject;
							}
						}
						else
						{
							mouseUp_selected[1] = true;
							activeBM = box;
							active = box.gameObject;
						}
					}


					if (box.myEvents.mouseHover_track)
					{
						Box_Event.OnMouseHover m = new Box_Event.OnMouseHover();
						m.position = MousePosition;
						box.Box_OnMouseHover(m);
					}
					if (box.myEvents.mouseEnter_track && ( !box.myEvents.mouseHover_track ))
					{
						box.myEvents.mouseHover_track = true;
					}
					if (( !box.myEvents.mouseEnter_track ) && ( !box.myEvents.mouseHover_track ))
					{
						box.myEvents.mouseEnter_track = true;

						Box_Event.OnMouseEnter m = new Box_Event.OnMouseEnter();
						m.position = MousePosition;
						box.Box_OnMouseEnter(m);
					}
				}
				else
				{
					if (box.myEvents.mouseExit_track)
						box.myEvents.mouseExit_track = false;

					if (!box.myEvents.mouseExit_track)
					{
						if (box.myEvents.mouseHover_track || box.myEvents.mouseEnter_track)
						{
							box.myEvents.mouseExit_track = true;

							Box_Event.OnMouseExit m = new Box_Event.OnMouseExit();
							m.position = MousePosition;
							box.Box_OnMouseExit(m);
						}
					}

						

					//if (box.mouseEnter_track)
					box.myEvents.mouseEnter_track = false;
					
					//if (box.mouseHover_track)
					box.myEvents.mouseHover_track = false;
				}
			}
		}

#if (UNITY_EDITOR && !TESTPLAY)
		if (MouseInGameArea)
			UnityEditor.Selection.activeGameObject = active;
#endif

		if (mouseDown_selected[0] || mouseDown_selected[1] || mouseDown_selected[2])
		{
			bool ok = false;
			Box_Event.OnMouseDown m = new Box_Event.OnMouseDown();

			if (( activeBM.myEvents.mouseDownTimer[0] > activeBM.property.MouseDownTimeDelay[0] ) && ( ( MouseButton & Box_Event.MouseButtonLeft ) != 0 ) && mouseDown_selected[0])
			{
				activeBM.myEvents.mouseDownTimer[0] = 0f;
				m.time[0] = lastTime;
				m.deltaTime[0] = activeBM.myEvents.mouseDownTimer[0];
				mouseDown_track[0] = true;
				ok = true;
			}

			if (( activeBM.myEvents.mouseDownTimer[1] > activeBM.property.MouseDownTimeDelay[1] ) && ( ( MouseButton & Box_Event.MouseButtonRight ) != 0 ) && mouseDown_selected[1])
			{
				activeBM.myEvents.mouseDownTimer[1] = 0f;
				m.time[1] = lastTime;
				m.deltaTime[1] = activeBM.myEvents.mouseDownTimer[1];
				mouseDown_track[1] = true;
				ok = true;
			}

			if (( activeBM.myEvents.mouseDownTimer[2] > activeBM.property.MouseDownTimeDelay[2] ) && ( ( MouseButton & Box_Event.MouseButtonMiddle ) != 0 ) && mouseDown_selected[2])
			{
				activeBM.myEvents.mouseDownTimer[2] = 0f;
				m.time[2] = lastTime;
				m.deltaTime[2] = activeBM.myEvents.mouseDownTimer[2];
				mouseDown_track[2] = true;
				ok = true;
			}

			if (ok)
			{
//				if (activeBM.myPage != null)
//				{
				//Box_Page.FocusedPage = activeBM.myPage;
				Box_Page.SetFocus(activeBM.myPage);
//				}

				m.position = MousePosition;
				m.button = MouseButton;
				activeBM.Box_OnMouseDown(m);
			}
		}

		if (mouseUp_selected[0] || mouseUp_selected[1] || mouseUp_selected[2])
		{
			bool ok = false;
			Box_Event.OnMouseUp m = new Box_Event.OnMouseUp();

			if (( activeBM.myEvents.mouseUpTimer[0] > activeBM.property.MouseUpTimeDelay[0] ) && ( ( MouseButton & Box_Event.MouseButtonLeft ) != 0 ) && mouseUp_selected[0])
			{
				activeBM.myEvents.mouseUpTimer[0] = 0f;
				m.time[0] = lastTime;
				m.deltaTime[0] = activeBM.myEvents.mouseDownTimer[0];
				mouseUp_track[0] = true;
				ok = true;
			}

			if (( activeBM.myEvents.mouseUpTimer[1] > activeBM.property.MouseUpTimeDelay[1] ) && ( ( MouseButton & Box_Event.MouseButtonRight ) != 0 ) && mouseUp_selected[1])
			{
				activeBM.myEvents.mouseUpTimer[1] = 0f;
				m.time[1] = lastTime;
				m.deltaTime[1] = activeBM.myEvents.mouseDownTimer[1];
				mouseUp_track[1] = true;
				ok = true;
			}

			if (( activeBM.myEvents.mouseUpTimer[2] > activeBM.property.MouseUpTimeDelay[2] ) && ( ( MouseButton & Box_Event.MouseButtonMiddle ) != 0 ) && mouseUp_selected[2])
			{
				activeBM.myEvents.mouseUpTimer[2] = 0f;
				m.time[2] = lastTime;
				m.deltaTime[2] = activeBM.myEvents.mouseDownTimer[2];
				mouseUp_track[2] = true;
				ok = true;
			}

			if (ok)
			{
//				if (activeBM.myPage != null)
//				{
				//Box_Page.FocusedPage = activeBM.myPage;
				Box_Page.SetFocus(activeBM.myPage);
//				}

				m.position = MousePosition;
				m.button = MouseButton;
				activeBM.Box_OnMouseUp(m);
			}
		}

#if (UNITY_EDITOR && !TESTPLAY)
		if (Event.current.type == EventType.mouseUp)
#else
		if (isMouseUp[0])
#endif
		{
			mouseDown_track[0] = false;
		}
#if (UNITY_EDITOR && !TESTPLAY)
		if (Event.current.type == EventType.mouseUp)
#else
		if (isMouseUp[1])
#endif
		{
			mouseDown_track[1] = false;
		}
#if (UNITY_EDITOR && !TESTPLAY)
		if (Event.current.type == EventType.mouseUp)
#else
		if (isMouseUp[2])
#endif
		{
			mouseDown_track[2] = false;
		}

#if (UNITY_EDITOR && !TESTPLAY)
		if (Event.current.type == EventType.mouseDown)
#else
		if (isMouseDown[0])
#endif
		{
			mouseUp_track[0] = false;
		}

#if (UNITY_EDITOR && !TESTPLAY)
		if (Event.current.type == EventType.mouseDown)
#else
		if (isMouseDown[1])
#endif
		{
			mouseUp_track[1] = false;
		}

#if (UNITY_EDITOR && !TESTPLAY)
		if (Event.current.type == EventType.mouseDown)
#else
		if (isMouseDown[2])
#endif
		{
			mouseUp_track[2] = false;
		}
	}
}