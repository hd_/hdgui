using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Box_Helper_Alignment : MonoBehaviour
{
	public enum HAlign
	{
		Left,
		Middle,
		Right
	}
	;
	public enum VAlign
	{
		Top,
		Center,
		Bottom
	}
	;

	public HAlign HorizontalAlign = HAlign.Middle;
	public VAlign VerticalAlign = VAlign.Center;
	private Box_Model BM;

	void Align ()
	{
		BM.PositionUnit = Box_Unit.Type.Percent;
		BM.PivotUnit = Box_Unit.Type.Percent;
		switch ( HorizontalAlign )
		{
			case HAlign.Left:
				BM.Pivot.x = 0f;
				BM.Position.x = 0f;
				break;
			case HAlign.Middle:
				BM.Pivot.x = 0.5f;
				BM.Position.x = 0.5f;
				break;
			case HAlign.Right:
				BM.Pivot.x = 1f;
				BM.Position.x = 1f;
				break;
		}
		switch ( VerticalAlign )
		{
			case VAlign.Top:
				BM.Pivot.y = 1f;
				BM.Position.y = 1f;
				break;
			case VAlign.Center:
				BM.Pivot.y = 0.5f;
				BM.Position.y = 0.5f;
				break;
			case VAlign.Bottom:
				BM.Pivot.y = 0f;
				BM.Position.y = 0f;
				break;
		}
	}

	// Use this for initialization
	void Start ()
	{
		BM = gameObject.GetComponent<Box_Model>();
		Align();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (BM == null)
			BM = gameObject.GetComponent<Box_Model>();

		Align();	
	}
}
