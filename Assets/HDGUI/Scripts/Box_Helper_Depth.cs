﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Box_Helper_Depth : MonoBehaviour
{
	public bool InheritDepthBase = true;
	public float BasePlus = 1f;
	public float DepthSum;

	private Box_Model BM;
	private Box_Helper_Page BP;

	// Use this for initialization
	void Start ()
	{
		BM = gameObject.GetComponent<Box_Model>();
		if (BM != null)
			if (BM.GetParent != null)
			{
				Vector3 p = BM.GetParent.transform.localPosition;
				Vector3 l = gameObject.transform.localPosition;
				DepthSum = p.z + BasePlus;
				gameObject.transform.localPosition = new Vector3(l.x, l.y, DepthSum);
			}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (BP == null)
			BP = gameObject.GetComponent<Box_Helper_Page>();

		if (BP != null)
			if (BP.Page != null)
				if (( BP.Page.isFocused ) && ( BP.PageRoot ))
					return; // letting Box_Page take over depth control.

		if (InheritDepthBase)
		{
			BM = gameObject.GetComponent<Box_Model>();
			if (BM != null)
				if (BM.GetParent != null)
				{
					Vector3 p = BM.GetParent.transform.localPosition;
					Vector3 l = gameObject.transform.localPosition;
					DepthSum = p.z + BasePlus;
					gameObject.transform.localPosition = new Vector3(l.x, l.y, DepthSum);
				}
		}
		else
		{
			BasePlus = 0f;
			Vector3 l = gameObject.transform.localPosition;
			gameObject.transform.localPosition = new Vector3(l.x, l.y, DepthSum);
		}
		
	}
}
