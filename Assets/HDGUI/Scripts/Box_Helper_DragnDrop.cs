﻿using UnityEngine;
using System.Collections;

//[ExecuteInEditMode]
public class Box_Helper_DragnDrop : MonoBehaviour
{
	private Box_Model BM;
	private bool drag = false;
	private bool dragged = false;
	private Vector2 position;
	private Vector2 delta;

	public bool Smooth = true;
	public float SmoothSpeed = 30f;

	public bool UsePercentage = true; 

	public bool LockPageOnDrag = true;
	public bool ModalPageOnDrag = true;

	public bool DragDelay = true;
	public float DragDelayTime = 0.1f;
	private float dragStartTime = 0f;
	private bool dragDelayOk = false;
	private Vector2 newDelta;

	public float SnapDistance = 0.000f;

	// Use this for initialization
	void Start ()
	{
		BM = gameObject.GetComponent<Box_Model>();
		BM.Register(mOnMouseDown);

		Box_Helper_Alignment BA = gameObject.GetComponent<Box_Helper_Alignment>();
		if (BA != null)
			if (BA.enabled == true)
			{
				Debug.Log("An active Box_Helper_Alignment is conflicting with Box_Helper_DragnDrop. Disabling it.");
				BA.enabled = false;
			}
//		BM.Register(mOnMouseUp);
	}

	void mOnMouseDown ( Box_Event.OnMouseDown e )
	{
		if (e.button == Box_Event.MouseButtonLeft)
		//if (e.GetPressed(Box_Event.MouseButtonLeft) && e.GetPressed(Box_Event.MouseButtonRight)) // example of drag only when these two buttons pressed
		{
			DragStart(e.position);
		}
	}

//	void mOnMouseUp(Box_Event.OnMouseUp e)
//	{
//		if (e.button == Box_Event.MouseButtonLeft)
//		{
//			drag = false;
//		}
//	}

//	void mOnMouseHover(Box_Event.OnMouseHover e)
//	{
//		if (drag)
//		{
//			position = e.position;
//		}
//	}

	// Update is called once per frame
	void Update ()
	{

		if (BM == null)
		{
			BM = gameObject.GetComponent<Box_Model>();
			BM.Register(mOnMouseDown);
//			BM.Register(mOnMouseUp);
//			BM.Register(mOnMouseHover);
		}
		if (BM != null)
		{
			Drag();

			if (dragged) 
			{// Actual movement of the box being dragged.
				if (!drag && (Vector2.Distance(new Vector2 (BM.Position.x,BM.Position.y),position+delta) <= SnapDistance) )
				{
					BM.Position.x = position.x + delta.x;
					BM.Position.y = position.y + delta.y;
				}

				if (Smooth)
				{
					BM.Position.x = Mathf.Lerp(BM.Position.x,position.x+delta.x,SmoothSpeed * Time.deltaTime);
					BM.Position.y = Mathf.Lerp(BM.Position.y,position.y+delta.y,SmoothSpeed * Time.deltaTime);
				}
				else
				{
					BM.Position.x = position.x+delta.x;
					BM.Position.y = position.y+delta.y;
				}
			}
		}
	}

	void DragStart(Vector2 MousePosition)
	{
		dragStartTime = Time.realtimeSinceStartup;

		drag = true;
		dragDelayOk = false;

		if (BM.myPage != null)
		{
			GameObject GO = BM.myPage.GetPageRoot;
			if (GO != null)
			{
				if (LockPageOnDrag)
					GO.GetComponent<Box_Helper_Page>().Locked = true;
				if (ModalPageOnDrag)
					GO.GetComponent<Box_Helper_Page>().Modal = true;
			}
		}

		Box_Rect r = BM.GetBox.GetPosition();
		Box_Rect p = BM.GetBox.GetPivotInPixels();
		if (UsePercentage)
		{
			float w = Box_Util.BoxScreen.width.InPixels();
			float h = Box_Util.BoxScreen.height.InPixels();
			newDelta = new Vector2((r.x.InPixels()/w + p.x.InPixels()/w) - MousePosition.x/w,
			                    (r.y.InPixels()/h + p.y.InPixels()/h) - MousePosition.y/h );
			if (BM.PositionUnit != Box_Unit.Type.Percent)
			{
				BM.PositionUnit = Box_Unit.Type.Percent;
				BM.Position.x /= w;
				BM.Position.y /= h;
			}
		}
		else
		{
			delta = new Vector2((r.x.InPixels() + p.x.InPixels()) - MousePosition.x,
			                    (r.y.InPixels() + p.y.InPixels()) - MousePosition.y );
			BM.PositionUnit = Box_Unit.Type.Pixel;
			BM.Position.x = r.x.InPixels() + p.y.InPixels();
			BM.Position.y = r.y.InPixels() + p.y.InPixels();
		}
	}

	void Drag()
	{
		if (drag)
		{
			if (!Input.GetMouseButton(0))
			{
				DragDrop();
				return; //don't process any further;
			}

			Vector2 MousePosition =  Box_Controller.GetMousePosition();

			if (DragDelay)
			{
				if (!dragDelayOk)
				{
					if (Time.realtimeSinceStartup - dragStartTime > DragDelayTime )
					{
						dragged = true; // it's ok to start visually moving the dragged box
						dragDelayOk = true;

						delta = newDelta;

					}
					else
						return; // don't process any further;
				}
			}

			position = MousePosition;
			if (UsePercentage)
			{
				float w = Box_Util.BoxScreen.width.InPixels();
				float h = Box_Util.BoxScreen.height.InPixels();
				position.x /= w;
				position.y /= h;
			}

		}
	}

	void DragDrop()
	{
		drag=false;
		dragDelayOk = false;
		
		if (BM.myPage != null)
		{
			GameObject GO = BM.myPage.GetPageRoot;
			if (GO != null)
			{
				if (LockPageOnDrag)
					GO.GetComponent<Box_Helper_Page>().Locked = false;
				if (ModalPageOnDrag)
					GO.GetComponent<Box_Helper_Page>().Modal = false;
			}
		}
	}
}
