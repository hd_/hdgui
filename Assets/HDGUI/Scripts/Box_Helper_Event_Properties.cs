﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Box_Helper_Event_Properties : MonoBehaviour
{
	public float[] MouseUpTimeDelay = {0.15f,0.15f,0.15f};
	public float[] MouseDownTimeDelay = {0.15f,0.15f,0.15f};
	public bool Selectable = true;

	private Box_Model BM;

	// Use this for initialization
	void Start ()
	{
		BM = gameObject.GetComponent<Box_Model>(); 
		
		if (BM != null)
			if (BM.property != null)
			{
				BM.property.MouseDownTimeDelay = MouseDownTimeDelay;
				BM.property.MouseUpTimeDelay = MouseUpTimeDelay;
				BM.property.Selectable = Selectable;
			}
	}

	// Update is called once per frame
	void Update ()
	{
		if (BM == null)
			BM = gameObject.GetComponent<Box_Model>();

		if (BM != null)
			if (BM.property != null)
			{
				BM.property.MouseDownTimeDelay = MouseDownTimeDelay;
				BM.property.MouseUpTimeDelay = MouseUpTimeDelay;
				BM.property.Selectable = Selectable;
			}
	}
}
