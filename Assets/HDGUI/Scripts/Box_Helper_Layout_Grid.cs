﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Box_Helper_Layout_Grid : MonoBehaviour
{
	private Box_Widget_GUISkin BM;
	public int Rows = 1;
	public int Columns = 1;
	public int RowIndex = 0;
	public int ColumnIndex = 0;

	// Use this for initialization
	void Start ()
	{
		CalcPosition();
	}
	
	// Update is called once per frame
	void Update ()
	{
		CalcPosition();
	}

	void CalcPosition ()
	{
		BM = gameObject.GetComponent<Box_Widget_GUISkin>();
		if (BM != null)
		{
			BM.PositionUnit = Box_Unit.Type.Percent;
			BM.PositionDimensionUnit = Box_Unit.Type.Percent;
			
			float row_unit = 1f / ( (float)Rows );
			float column_unit = 1f / ( (float)Columns );

			BM.Position.x = column_unit * (float)ColumnIndex;
			BM.Position.y = row_unit * (float)RowIndex;

			BM.Position.width = column_unit;
			BM.Position.height = row_unit;
		}
	}
}
