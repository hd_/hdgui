﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Box_Helper_Page : MonoBehaviour
{
	public Box_Page Page;
	public string PageName = "";
	public bool PageRoot = false;
	public bool Modal = false;
	public bool Active = true;
	public bool Locked = false;
	public bool Visible = true;

	public float RootFocusedDepth = 1000f;

	private float typeTimer = 5f;
	private float typeTimerDelay = 3f;
	private float deltaTime = 0f;
	private float lastTime = 0f;
	private string previousName = "";

	private Box_Page previousPage;

	private Box_Model BM;

	void Start ()
	{
		if (PageRoot)
			if (PageName != "")
			{
				Page = Box_Page.Register(PageName);
				Page.RegisterElement(gameObject);
				Page.SetRoot(gameObject);
			}

		BM = gameObject.GetComponent<Box_Model>();
		if (BM != null)
			BM.myPage = Page;
	}

	void Update ()
	{
		if (BM == null)
			BM = gameObject.GetComponent<Box_Model>();
		if (BM == null)
		{
			Debug.Log("No Box_Model attached to game object.");
			return;
		}
		//Determine which page we are on
		if (PageRoot)
		{
			if (lastTime < 1f)
				lastTime = Time.realtimeSinceStartup;

			deltaTime = Time.realtimeSinceStartup - lastTime;
			lastTime = Time.realtimeSinceStartup;
			if (PageName == previousName)
				typeTimer += deltaTime;
			else
			{
				previousName = PageName;
				typeTimer = 0f;
			}

			if (typeTimer >= typeTimerDelay)
			{
				if (previousName != PageName)
				{
					previousName = PageName;
					typeTimer = 0f;
				}
				Box_Page p = Box_Page.FindPage(PageName);
				if (p != null)
					Page = p;
				else
				if (PageName != "")
					{
						Page = Box_Page.Register(PageName);
						Page.SetRoot(gameObject);
					}
			}
		}
		else
		{
			if (BM.GetParent != null)
			{
				Box_Helper_Page p = BM.GetParent.GetComponent<Box_Helper_Page>();
				if (p != null)
				{
					Page = p.Page;
					if (Page != null)
						Page.RegisterElement(gameObject);
				}
			}
		}

		//Act on page state
		if (Page != null)
		{
			if (PageRoot)
			{
				Page.SetActive(Active);
				Page.SetModal(Modal);
				Page.SetLock(Locked);
				Page.SetVisible(Visible);
				Page.SetFocusedDepth(RootFocusedDepth);
			}
			else //if not page root, inherit page and state.
			{
				PageName = Page.Name;
				Active = Page.isActive;
				Modal = Page.isModal;
				Locked = Page.isLocked;
				Visible = Page.isVisible;
				RootFocusedDepth = Page.GetRootFocusedDepth;
			}
		}

		//if the object changes page, remove it from previous page.
		if (( Page != previousPage ) && ( previousPage != null ))
		{
			previousPage.UnregisterElement(gameObject);
		}
		previousPage = Page;
		BM.myPage = Page;

//		if (!Page.isActive && !PageRoot)
//		{
//			gameObject.SetActive(false);
//		}
	}

}
