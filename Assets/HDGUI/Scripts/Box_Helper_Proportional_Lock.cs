﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Box_Helper_Proportional_Lock : MonoBehaviour
{
	public float WidthProportion;
	public float HeightProportion;
	public bool MaintainArea = true;
	public bool WidthMajor = true;

#if UNITY_EDITOR 
	// Using an editor hack to remember what the original start width and height should be.
	public float StartWidth = 0f;
	public float StartHeight = 0f;
	public bool EnableInEditor = false;
	private bool wasEnabledInEditor = false;
#endif

	private Box_Model BM;
	private Box_Rect startDimension;

	// Use this for initialization
	void Start ()
	{

		BM = gameObject.GetComponent<Box_Model>();
		if (BM != null)
		{
			startDimension = new Box_Rect();
			startDimension.width.SetValue(BM.Position.width, BM.PositionDimensionUnit);
			startDimension.height.SetValue(BM.Position.height, BM.PositionDimensionUnit);
		}

#if UNITY_EDITOR
		if (!EnableInEditor)
		{
			if (Mathf.Abs(StartWidth - BM.Position.width) > 0.001f)
				StartWidth = BM.Position.width;

			if (Mathf.Abs(StartHeight - BM.Position.height) > 0.001f)
				StartHeight = BM.Position.height;

			return;
		}
		wasEnabledInEditor = true;
#endif
		AutoSize();
	}
	
	// Update is called once per frame
	void Update ()
	{
		BM = gameObject.GetComponent<Box_Model>();
#if UNITY_EDITOR
		if (!EnableInEditor)
		{
			if (wasEnabledInEditor)
			{
				wasEnabledInEditor = false;
				if (BM != null)
				{
					BM.Position.width = StartWidth;
					BM.Position.height = StartHeight;
				}
			}
			else
			{
				if (Mathf.Abs(StartWidth - BM.Position.width) > 0.001f)
					StartWidth = BM.Position.width;
			
				if (Mathf.Abs(StartHeight - BM.Position.height) > 0.001f)
					StartHeight = BM.Position.height;
			}
			
			return;
		}
		wasEnabledInEditor = true;

		if (BM != null)
		{
			if (BM != null)
			{
				startDimension = new Box_Rect();
				startDimension.width.SetValue(StartHeight, BM.PositionDimensionUnit);
				startDimension.height.SetValue(StartWidth, BM.PositionDimensionUnit);
			}
		}
#endif

		AutoSize();
	}

	void AutoSize ()
	{
		if (BM == null)
		{
			BM = gameObject.GetComponent<Box_Model>();
			if (BM != null)
			{
				startDimension = new Box_Rect();
				startDimension.width.SetValue(BM.Position.width, BM.PositionDimensionUnit);
				startDimension.height.SetValue(BM.Position.height, BM.PositionDimensionUnit);
			}
		}

		if (BM != null)
		{
			if (BM.PositionDimensionUnit == Box_Unit.Type.Percent)
			{   //percent:
				float f_wh = Box_Util.BoxScreen.width.InPixels() / Box_Util.BoxScreen.height.InPixels();
				if (MaintainArea)
				{   //Maintain scale to screen size:
//					float max = Mathf.Max(startDimension.width.value, startDimension.height.value);
					if (f_wh > 1f / f_wh)
					{ // screen width is greater than screen height
						if (WidthMajor)
						{
							BM.Position.width = startDimension.width.value;
							BM.Position.height = startDimension.width.value * ( HeightProportion / WidthProportion ) * f_wh;
						}
						else
						{
							BM.Position.width = startDimension.height.value * ( WidthProportion / HeightProportion );
							BM.Position.height = startDimension.height.value * f_wh;
						}
					}
					else
					{ //screen height is greater than screen width
						if (WidthMajor)
						{
							BM.Position.width = startDimension.width.value * 1f / f_wh;
							BM.Position.height = startDimension.width.value * ( HeightProportion / WidthProportion );
						}
						else
						{
							BM.Position.width = startDimension.height.value * ( WidthProportion / HeightProportion ) * 1f / f_wh;
							BM.Position.height = startDimension.height.value;
						}
					}
				}
				else //Scale down with screen size:
				{
					if (WidthMajor)
					{
						BM.Position.width = startDimension.width.value;
						BM.Position.height = startDimension.width.value * ( HeightProportion / WidthProportion ) * f_wh;
					}
					else
					{
						BM.Position.width = startDimension.height.value * ( WidthProportion / HeightProportion ) * 1f / f_wh;
						BM.Position.height = startDimension.height.value;
					}
				}
			}
			else // pixels:
			{
				if (WidthMajor)
				{
					BM.Position.width = startDimension.width.value;
					BM.Position.height = startDimension.width.value * ( HeightProportion / WidthProportion );
				}
				else
				{
					BM.Position.width = startDimension.height.value * ( WidthProportion / HeightProportion );
					BM.Position.height = startDimension.height.value;
				}
			}
		}
	}
}
