﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Box_Helper_Style_Share : MonoBehaviour
{
	private Box_Widget_GUISkin BM;
	public Box_Widget_GUISkin[] BMs;

	// Use this for initialization
	void Start ()
	{
		BM = gameObject.GetComponent<Box_Widget_GUISkin>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		foreach (Box_Widget_GUISkin bm in BMs)
		{
			bm.Active = BM.Active;
			bm.Hover = BM.Hover;
		}
	}
}
