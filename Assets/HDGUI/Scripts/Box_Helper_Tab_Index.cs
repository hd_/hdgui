﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Box_Helper_Tab_Index : MonoBehaviour
{
	public int TabIndex = 0;

	private Box_Model BM;

	// Use this for initialization
	void Start ()
	{
		BM = gameObject.GetComponent<Box_Model>();
	}
	
	// Update is called once per frame
	void Update ()
	{

	}
}
