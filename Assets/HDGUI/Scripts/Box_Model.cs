﻿//#define EVENTLOG
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class Box_Model : MonoBehaviour
{
	public Rect Position;
	public Box_Unit.Type PositionUnit = Box_Unit.Type.Pixel;
	public Box_Unit.Type PositionDimensionUnit = Box_Unit.Type.Percent;
	public Rect Margin;
	public Box_Unit.Type MarginUnit = Box_Unit.Type.Percent;
	public Rect Border;
	public Box_Unit.Type BorderUnit = Box_Unit.Type.Pixel;
	public Rect Padding;
	public Box_Unit.Type PaddingUnit = Box_Unit.Type.Percent;

	public Vector2 Pivot;
	public Box_Unit.Type PivotUnit;
	//public Box.Alignment.Vertical VerticalAlignment;
	//public Box.Alignment.Horizontal HorizontalAlignment;


	protected Box pBox;
	protected Box myBox;
	protected GameObject parent;

	public Box_Event_Property property;

	private List<Box_Event.D_OnMouseDown> D_OnMouseDown;
	private List<Box_Event.D_OnMouseEnter> D_OnMouseEnter;
	private List<Box_Event.D_OnMouseExit> D_OnMouseExit;
	private List<Box_Event.D_OnMouseHover> D_OnMouseHover;
	private List<Box_Event.D_OnMouseUp> D_OnMouseUp;

	public Box_Event myEvents;

	public Box_Page myPage;

	public Box GetBox
	{
		get{ return myBox;}
	}

	public GameObject GetParent
	{
		get{ return parent;}
	}

	public Box GetParentBox
	{
		get { return pBox;}
	}

	public Box_Model GetParentBoxModel
	{
		get
		{
			if (parent == null)
				return null;
			Box_Model p = parent.GetComponent<Box_Model>();
			if (p == null)
				return null;
			return p;
		}
	}

	protected Box_Rect box_position
	{
		get
		{
			Box_Unit x, y, width, height;
			x = new Box_Unit(Position.x, PositionUnit);
			y = new Box_Unit(Position.y, PositionUnit);
			width = new Box_Unit(Position.width, PositionDimensionUnit);
			height = new Box_Unit(Position.height, PositionDimensionUnit);
			if (pBox != null)
			{
				Box_Rect p = pBox.GetContentPosition();
				width.Parent = new Box_Unit(p.width.InPixels());
				height.Parent = new Box_Unit(p.height.InPixels());
			}
			return new Box_Rect(x, y, width, height);
		}
	}
	protected Box_Rel box_margin
	{
		get
		{
			Box_Unit left, top, right, bottom;
			left = new Box_Unit(Margin.x, MarginUnit);
			top = new Box_Unit(Margin.y, MarginUnit);
			right = new Box_Unit(Margin.width, MarginUnit);
			bottom = new Box_Unit(Margin.height, MarginUnit);
			if (pBox != null)
			{
				Box_Rect p = pBox.GetContentPosition();
				right.Parent = new Box_Unit(p.width.InPixels());
				bottom.Parent = new Box_Unit(p.height.InPixels());
			}
			return new Box_Rel(left, top, right, bottom);
		}
	}
	protected Box_Rel box_border
	{
		get
		{
			Box_Unit left, top, right, bottom;
			left = new Box_Unit(Border.x, BorderUnit);
			top = new Box_Unit(Border.y, BorderUnit);
			right = new Box_Unit(Border.width, BorderUnit);
			bottom = new Box_Unit(Border.height, BorderUnit);
			if (pBox != null)
			{
				Box_Rect p = pBox.GetContentPosition();
				right.Parent = new Box_Unit(p.width.InPixels());
				bottom.Parent = new Box_Unit(p.height.InPixels());
			}
			return new Box_Rel(left, top, right, bottom);
		}
	}
	protected Box_Rel box_padding
	{
		get
		{
			Box_Unit left, top, right, bottom;
			left = new Box_Unit(Padding.x, PaddingUnit);
			top = new Box_Unit(Padding.y, PaddingUnit);
			right = new Box_Unit(Padding.width, PaddingUnit);
			bottom = new Box_Unit(Padding.height, PaddingUnit);
			if (myBox != null)
			{
				Box_Rect p = myBox.GetBorderPosition();
				right.Parent = new Box_Unit(p.width.InPixels());
				bottom.Parent = new Box_Unit(p.height.InPixels());
			}
			return new Box_Rel(left, top, right, bottom);
		}
	}

	protected Box_Rect CalcMyBoxPosition ()
	{
		Box_Rect b = myBox.GetPosition();

		b.x.Parent = Box_Util.BoxScreen.width;
		b.y.Parent = Box_Util.BoxScreen.height;
		b.width.Parent = Box_Util.BoxScreen.width;
		b.height.Parent = Box_Util.BoxScreen.height;

		return new Box_Rect(b.x, b.y, b.width, b.height);
	}
 
	void D_Event_Init ()
	{
		if (D_OnMouseUp == null)
			D_OnMouseUp = new List<Box_Event.D_OnMouseUp>();
		
		if (D_OnMouseDown == null)
			D_OnMouseDown = new List<Box_Event.D_OnMouseDown>();
		
		if (D_OnMouseEnter == null)
			D_OnMouseEnter = new List<Box_Event.D_OnMouseEnter>();
		
		if (D_OnMouseHover == null)
			D_OnMouseHover = new List<Box_Event.D_OnMouseHover>();
		
		if (D_OnMouseExit == null)
			D_OnMouseExit = new List<Box_Event.D_OnMouseExit>();
	}

	public virtual void BoxUpdateInit ()
	{
#if UNITY_EDITOR
		if (myBox == null)
			myBox = new Box();

		if (property == null)
			property = new Box_Event_Property();

		if (myEvents == null)
			myEvents = new Box_Event();

		D_Event_Init();
#endif

		//if (parent != null)
		//	if (pBox != null)
		//		myBox.SetParent(pBox)

		myBox.SetPivot(Pivot, PivotUnit);
		myBox.SetPosition(box_position);
		myBox.SetMargin(box_margin);
		myBox.SetBorder(box_border);
		myBox.SetPadding(box_padding);

	}

	protected virtual void CalcBoxPosition ()
	{
		//Finding Parent:
		Transform pTrans = gameObject.transform.parent;

		if (pTrans == null) // No Parent Transform
		{
			parent = null;
			//myBox.SetPosition(CalcMyBoxPosition());
			myBox.SetParent(null);
			myBox.CalcPosition();
			myBox.SetPosition(CalcMyBoxPosition());
		}
		else // We have a Parent Transform
		{
			parent = pTrans.gameObject;

			if (parent == null) // We have no parent object
			{
				myBox.SetParent(null);
				myBox.CalcPosition();
				myBox.SetPosition(CalcMyBoxPosition());
			}
			else // We have a parent object
			{
				Box_Model pBM = parent.GetComponent<Box_Model>();

				if (pBM == null) // We have no parent with Box_Model
				{
					myBox.SetParent(null);
					myBox.CalcPosition();
					myBox.SetPosition(CalcMyBoxPosition());
				}
				else //we have a parent with Box_Model
				{
					pBox = pBM.GetBox;

					if (pBox == null) //invalid parent -- not a Box_Model Parent?-
					{
						myBox.SetParent(null);
						myBox.CalcPosition();
						myBox.SetPosition(CalcMyBoxPosition());
					}
					else //we have a Box_Model Parent
					{
						myBox.SetParent(pBox);
						myBox.CalcPosition(); 
					}
				}
			}
		}
	}

	protected virtual void BoxUpdate ()
	{
		Box_Util.BoxScreenUpdate();
		BoxUpdateInit();
		CalcBoxPosition();
	}
		
	protected void BoxUpdateChildren ()
	{
		Transform[] children = GetComponentsInChildren<Transform>(); // Selecting all children in heirarchy
		Box_Model c;

		foreach (Transform child in children)
		{
			c = child.gameObject.GetComponent<Box_Model>(); 
			if (c != null) //Child object has Box_Model
			{
				c.BoxUpdate();
			}
			
		}
	}
	// Use this for initialization
	public virtual void Start ()
	{ 
//#if !UNITY_EDITOR
		myBox = new Box();
		property = new Box_Event_Property();
		myEvents = new Box_Event();

		D_OnMouseUp = new List<Box_Event.D_OnMouseUp>();
		D_OnMouseDown = new List<Box_Event.D_OnMouseDown>();
		D_OnMouseEnter = new List<Box_Event.D_OnMouseEnter>();
		D_OnMouseHover = new List<Box_Event.D_OnMouseHover>();
		D_OnMouseExit = new List<Box_Event.D_OnMouseExit>();
//#endif
		BoxUpdate();
		BoxUpdateChildren();
	}


	public void Register ( Box_Event.D_OnMouseUp method )
	{
		D_Event_Init();
		if (!D_OnMouseUp.Contains(method))
			D_OnMouseUp.Add(method);
	}
	
	public void Register ( Box_Event.D_OnMouseDown method )
	{
		D_Event_Init();
		if (!D_OnMouseDown.Contains(method))
			D_OnMouseDown.Add(method);
	}
	
	public void Register ( Box_Event.D_OnMouseEnter method )
	{
		D_Event_Init();
		if (!D_OnMouseEnter.Contains(method))
			D_OnMouseEnter.Add(method);
	}
	
	public void Register ( Box_Event.D_OnMouseHover method )
	{
		D_Event_Init();
		if (!D_OnMouseHover.Contains(method))
			D_OnMouseHover.Add(method);
	}
	
	public void Register ( Box_Event.D_OnMouseExit method )
	{
		D_Event_Init();
		if (!D_OnMouseExit.Contains(method))
			D_OnMouseExit.Add(method);
	}

	public void Unregister ( Box_Event.D_OnMouseUp method )
	{
		D_OnMouseUp.Remove(method);
	}
	
	public void Unregister ( Box_Event.D_OnMouseDown method )
	{
		D_OnMouseDown.Remove(method);
	}
	
	public void Unregister ( Box_Event.D_OnMouseEnter method )
	{
		D_OnMouseEnter.Remove(method);
	}
	
	public void Unregister ( Box_Event.D_OnMouseHover method )
	{
		D_OnMouseHover.Remove(method);
	}
	
	public void Unregister ( Box_Event.D_OnMouseExit method )
	{
		D_OnMouseExit.Remove(method);
	}



	public void Box_OnMouseDown (Box_Event.OnMouseDown e)
	{
		if (D_OnMouseDown != null)
		{
			foreach (Box_Event.D_OnMouseDown callback in D_OnMouseDown)
				callback(e);
		}
#if (UNITY_EDITOR && EVENTLOG)
		Debug.Log("Box_OnMouseDown Event");
#endif
	}

	public void Box_OnMouseUp (Box_Event.OnMouseUp e)
	{
		if (D_OnMouseUp != null)
		{
			foreach (Box_Event.D_OnMouseUp callback in D_OnMouseUp)
				callback(e);
		}
#if (UNITY_EDITOR && EVENTLOG)
		Debug.Log("Box_OnMouseUp Event");
#endif
	}

	public void Box_OnMouseEnter (Box_Event.OnMouseEnter e)
	{
		if (D_OnMouseEnter != null)
		{
			foreach (Box_Event.D_OnMouseEnter callback in D_OnMouseEnter)
				callback(e);
		}
#if (UNITY_EDITOR && EVENTLOG)
		Debug.Log("Box_OnMouseEnter Event");
#endif
	}

	public void Box_OnMouseExit (Box_Event.OnMouseExit e)
	{
		if (D_OnMouseExit != null)
		{
			foreach (Box_Event.D_OnMouseExit callback in D_OnMouseExit)
				callback(e);
		}
#if (UNITY_EDITOR && EVENTLOG)
		Debug.Log("Box_OnMouseExit Event");
#endif
	}

	public void Box_OnMouseHover (Box_Event.OnMouseHover e)
	{
		if (D_OnMouseHover != null)
		{
			foreach (Box_Event.D_OnMouseHover callback in D_OnMouseHover)
				callback(e);
		}
#if (UNITY_EDITOR && EVENTLOG)
		Debug.Log("Box_OnMouseHover Event");
#endif
	}

#if UNITY_EDITOR
	public virtual void OnGUI ()
	{
		BoxUpdate();
		BoxUpdateChildren();
	}
	public virtual void OnRenderObject ()
	{
		BoxUpdate();
		BoxUpdateChildren();
	}
#else
	public virtual void Update ()
	{
		BoxUpdate();
		BoxUpdateChildren();
	}
#endif
}
