﻿using UnityEngine;
using System.Collections;

public class Box_Widget : MonoBehaviour
{

	public GameObject AddGameObject ( string name )
	{
		GameObject GO =new GameObject(name);
		//GO.AddComponent<Transform>();
		GO.transform.parent= gameObject.transform;
		GO.transform.localScale = new Vector3(0,0,1);

		Box_Widget_GUISkin BW_GUISkin = GO.AddComponent<Box_Widget_GUISkin>();
		Box_Helper_Depth BHD = GO.AddComponent<Box_Helper_Depth>();
		GO.AddComponent<Box_Helper_Page>();
		Box_Helper_Alignment BHA = GO.AddComponent<Box_Helper_Alignment>();
//		BHA.VerticalAlign = Box_Helper_Alignment.VAlign.Center;
//		BHA.HorizontalAlign = Box_Helper_Alignment.HAlign.Middle;
		return GO;
	}
}
