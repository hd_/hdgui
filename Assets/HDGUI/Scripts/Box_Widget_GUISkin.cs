﻿using UnityEngine;
using System.Collections;

  
[ExecuteInEditMode]
public class Box_Widget_GUISkin: Box_Model
{
	public GUISkin skin;
	private GUITexture image;
	private GUIText text;
	private GUISkin parent_guiskin;
	private Box_Widget_GUISkin parent_widget_guiskin;
	public bool OriginalSize;
	public bool TextElement;
	public bool Active = false;
	public bool Hover = false;

	enum Styles
	{
		normal,
		hover,
		active,
		focused
	}
	;

	public void mOnMouseHover ( Box_Event.OnMouseHover e )
	{
		Hover = true;
	}

	public void mOnMouseExit ( Box_Event.OnMouseExit e )
	{
		Hover = false;
	}

	public void mOnMouseDown(Box_Event.OnMouseDown e)
	{
		//if (e.GetPressed(Box_Event.MouseButtonLeft))
		if (e.button == Box_Event.MouseButtonLeft)
		{
			Active = !Active;
		}
	}

	public GUISkin GetSkin()
	{

		if (skin==null)
		{
			if (parent_guiskin==null)
			{
				//Debug.Log("No skin assigned to Box_Widget_GUISkin");
				return null;
			}
			else
			{
				return parent_guiskin;
			}
		}
		else
		{
			return skin;
		}

	}

	public override void BoxUpdateInit()
	{
		base.BoxUpdateInit();
		if (parent_guiskin == null)
		{
			Transform p = gameObject.transform.parent;
			if (p != null)
			{
				parent_widget_guiskin = p.gameObject.GetComponent<Box_Widget_GUISkin>();
				if (parent_widget_guiskin != null)
					parent_guiskin = parent_widget_guiskin.GetSkin();
			}
		}

		Register(mOnMouseExit);
		Register(mOnMouseHover);
		Register(mOnMouseDown);

		if (TextElement)
		{
			text = gameObject.GetComponent<GUIText>();
			if (text == null)
			{
				text = gameObject.AddComponent<GUIText>();
				if (text != null)
				{
					text.font = GetSkin().box.font;
					text.fontStyle = GetSkin().box.fontStyle;
					text.color = GetSkin().box.normal.textColor;
					text.fontSize = GetSkin().box.fontSize;
				}
			}
		}
		else
		{
			image = gameObject.GetComponent<GUITexture>();
			if (image == null)
			{
				image = gameObject.AddComponent<GUITexture>();
				if (image != null)
					image.texture = GetSkin().box.normal.background;
			}
		}
//#endif
	}

	protected override  void CalcBoxPosition ()
	{

		if (TextElement)
		{
			text = gameObject.GetComponent<GUIText>();
			
			Rect r = text.GetScreenRect();
			Box_Unit x, y, width, height;
			x = new Box_Unit(Position.x, PositionUnit);
			y = new Box_Unit(Position.y, PositionUnit);
			width = new Box_Unit(r.width, Box_Unit.Type.Pixel);
			height = new Box_Unit(r.height, Box_Unit.Type.Pixel);
			if (pBox != null)
			{
				Box_Rect p = pBox.GetContentPosition();
				width.Parent = new Box_Unit(p.width.InPixels());
				height.Parent = new Box_Unit(p.height.InPixels());
			}
			Box_Rect b = new Box_Rect(x, y, width, height);
			myBox.SetPosition(b);
		}
		base.CalcBoxPosition();
		
	}

	protected override void BoxUpdate ()
	{ 
		base.BoxUpdate();
//#if UNITY_EDITOR
//		Register(mOnMouseExit);
//		Register(mOnMouseHover);
//		Register(mOnMouseDown);
//
//		image = gameObject.GetComponent<GUITexture>();
//		if (image == null)
//		{
//			image = gameObject.AddComponent<GUITexture>();
//			image.texture = skin.box.normal.background;
//		}
//#endif
		

		if ((parent_guiskin != null)&&(skin ==null))
		{
			Active = parent_widget_guiskin.Active;
			Hover = parent_widget_guiskin.Hover;
		}

		if ((parent_guiskin == null ) && (skin ==  null))
			return;

		if (TextElement)
		{
			if (myPage != null)
				text.enabled = myPage.isVisible;

			if (Active)
				text.color = GetSkin().box.active.textColor;
			else
				if (Hover)
					text.color = GetSkin().box.hover.textColor;
				else
					text.color = GetSkin().box.normal.textColor;

			Box_Rect b = myBox.GetBorderPosition();
			//Box_Rect c = myBox.GetContentPosition();
			Rect r = text.GetScreenRect();
			text.pixelOffset = new Vector2(b.x.InPixels(), b.y.InPixels() + r.height);
			//text.fontSize = (int)c.height.InPixels();
		}
		else
		{
			if (myPage != null)
				image.enabled = myPage.isVisible;

			if (Active)
				image.texture = GetSkin().box.active.background;
			else
				if (Hover)
					image.texture = GetSkin().box.hover.background;
				else
					image.texture = GetSkin().box.normal.background;

			Box_Rel b = myBox.GetBorder();
			image.border = new RectOffset((int)b.left.InPixels(), (int)b.top.InPixels(), (int)b.right.InPixels(), (int)b.bottom.InPixels());
			Box_Rect c = myBox.GetBorderPosition();
			image.pixelInset = new Rect(c.x.InPixels(), c.y.InPixels(), c.width.InPixels(), c.height.InPixels());
		}
	}
		
}
