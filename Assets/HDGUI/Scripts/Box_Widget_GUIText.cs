﻿using UnityEngine;
using System.Collections;

  
[ExecuteInEditMode]
public class Box_Widget_GUIText : Box_Model
{
	private GUIText text;

	protected override  void CalcBoxPosition ()
	{

		text = gameObject.GetComponent<GUIText>();
		
		Rect r = text.GetScreenRect();
		Box_Unit x, y, width, height;
		x = new Box_Unit(Position.x, PositionUnit);
		y = new Box_Unit(Position.y, PositionUnit);
		width = new Box_Unit(r.width, Box_Unit.Type.Pixel);
		height = new Box_Unit(r.height, Box_Unit.Type.Pixel);
		if (pBox != null)
		{
			Box_Rect p = pBox.GetContentPosition();
			width.Parent = new Box_Unit(p.width.InPixels());
			height.Parent = new Box_Unit(p.height.InPixels());
		}
		Box_Rect b = new Box_Rect(x, y, width, height);
		myBox.SetPosition(b);

		base.CalcBoxPosition();

	}

	protected override void BoxUpdate ()
	{
		//Debug.Log ("OVERRIDE BOXUPDATE");
//		Box_Rect r = CalcBoxPosition();
//		myBox.SetPosition(r);
//		if (!IgnoreDimensions)
//		{
//			myBox.SetMargin(box_margin);
//			myBox.SetBorder(box_border);
//			myBox.SetPadding(box_padding);
//		}
//		else
//		{
//			Box_Rel z = new Box_Rel();
//			myBox.SetMargin(z);
//			myBox.SetBorder(z);
//			myBox.SetPadding(z);
//		}

//		Rect r = text.GetScreenRect();
//		Box_Unit bx, by, bwidth, bheight;
//		bx = new Box_Unit(r.x, Box_Unit.Type.Pixel);
//		by = new Box_Unit(r.y, Box_Unit.Type.Pixel);
//		bwidth = new Box_Unit(r.width, Box_Unit.Type.Pixel);
//		bheight = new Box_Unit(r.height, Box_Unit.Type.Pixel);
//		Box_Rect nr = new Box_Rect(bx, by, bwidth, bheight);
//		myBox.SetPosition(nr);

		base.BoxUpdate();

		if (myPage != null)
			text.enabled = myPage.isVisible;

		Box_Rect b = myBox.GetBorderPosition();
		//Box_Rect c = myBox.GetContentPosition();
		Rect r = text.GetScreenRect();
		text.pixelOffset = new Vector2(b.x.InPixels(), b.y.InPixels() + r.height);
		//text.fontSize = (int)c.height.InPixels();
	}
		
}
