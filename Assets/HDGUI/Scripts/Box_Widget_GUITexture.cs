﻿using UnityEngine;
using System.Collections;

  
[ExecuteInEditMode]
public class Box_Widget_GUITexture: Box_Model
{
	private GUITexture image;
	public bool OriginalSize;

	protected override void CalcBoxPosition ()
	{
		//Debug.Log ("OVERRIDE CALCBOX");
		image = gameObject.GetComponent<GUITexture>();
		if (OriginalSize)
		{ 
			gameObject.transform.localScale = new  Vector3(0, 0, 1);
			Position.width = image.texture.width;
			Position.height = image.texture.height;
			PositionDimensionUnit = Box_Unit.Type.Pixel;
		}
		base.CalcBoxPosition();

	}

	protected override void BoxUpdate ()
	{ 
		//Debug.Log ("OVERRIDE BOXUPDATE");
//		Box_Rect r = CalcBoxPosition();
//		myBox.SetPosition(r);
//		if (!IgnoreDimensions)
//		{
//			myBox.SetMargin(box_margin);
//			myBox.SetBorder(box_border);
//			myBox.SetPadding(box_padding);
//		}
//		else
//		{
//			Box_Rel z = new Box_Rel();
//			myBox.SetMargin(z);
//			myBox.SetBorder(z);
//			myBox.SetPadding(z);
//		}

		base.BoxUpdate();

		if (myPage != null)
			image.enabled = myPage.isVisible;

		Box_Rel b = myBox.GetBorder();
		image.border = new RectOffset((int)b.left.InPixels(), (int)b.top.InPixels(), (int)b.right.InPixels(), (int)b.bottom.InPixels());
		Box_Rect c = myBox.GetBorderPosition();
		image.pixelInset = new Rect(c.x.InPixels(), c.y.InPixels(), c.width.InPixels(), c.height.InPixels());
	}
		
}
