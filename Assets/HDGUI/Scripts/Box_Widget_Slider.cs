﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Box_Widget_Slider : Box_Widget
{

	private Box_Model BM;

	private Box_Model WH_BM;
	private Box_Model WB_BM;

	public GameObject WidgetBar;
	public GameObject WidgetHandle;

	private bool drag = false;

	// Use this for initialization
	void Start ()
	{
		CreateWidgets();
	}

	public void mOnMouseDown ( Box_Event.OnMouseDown e )
	{
		drag = true;
		WidgetHandle.GetComponent<Box_Helper_Alignment>().enabled = false;
		WidgetHandle.GetComponent<Box_Helper_Layout_Grid>().enabled = false;
		Debug.Log("Hello from Handle !");
	}
	
	// Update is called once per frame
	void Update ()
	{
		CreateWidgets();

		if (drag)
		{
			if (!Input.GetMouseButton(0))
			{
				drag=false;
				return;
			}

			Vector2 MousePosition = Box_Controller.GetMousePosition();
			WH_BM.PositionUnit = Box_Unit.Type.Pixel;
			WH_BM.Position.x = MousePosition.x;
			WH_BM.Position.y  = MousePosition.y;
		}
	}

	void CreateWidgets()
	{
		if (BM == null)
			BM = gameObject.GetComponent<Box_Model>();

		if (WidgetBar == null)
		{
			WidgetBar = AddGameObject("Widget_Slider_Bar");
			
		}
		
		if (WidgetHandle == null)
		{
			WidgetHandle = AddGameObject("Widget_Slider_Handle");
			Box_Helper_Layout_Grid BHLG = WidgetHandle.AddComponent<Box_Helper_Layout_Grid>();
			BHLG.Rows = 1;
			BHLG.Columns = 10;
			BHLG.ColumnIndex = 9;
		}

		if (WH_BM == null)
		{
			WH_BM = WidgetHandle.GetComponent<Box_Model>();
			WH_BM.Register(mOnMouseDown);
		}
	}
}
