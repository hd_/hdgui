﻿using UnityEngine;
using System.Collections;


public class Box
{
	private Box_Rect Position;
	private Box_Rel Margin;
	private Box_Rel Border;
	private Box_Rel Padding;
//	private Box_Rel Content;

	private Box_Rect rPosition;
	private Box_Rect rMargin;
	private Box_Rect rBorder;
	private Box_Rect rPadding;
	private Box_Rect rContent;

//	public bool IgnoreDimensions = false; 

	private Box Parent;

	public struct Alignment
	{
		public enum Horizontal
		{
			Left,
			Middle,
			Right}
		;
		public enum Vertical
		{
			Bottom,
			Center,
			Top}
		;
	}

	//private Alignment alignment;
	private Box_Rect pivot;

//	public Alignment SetAlignment
//	{
//		set{ alignment = value;}
//	}
	
	public Box ()
	{
		//Debug.Log ("BOX CONSTRUCTOR0");
		Position = new Box_Rect();
		Margin = new Box_Rel();
		Border = new Box_Rel();
		Padding = new Box_Rel();
//		Content = new Box_Rel();

		rPosition = new Box_Rect();
		rMargin = new Box_Rect();
		rBorder = new Box_Rect();
		rPadding = new Box_Rect();

		pivot = new Box_Rect();

		SetPosition(Box_Util.BoxRectZero);
		SetMargin(Box_Util.BoxRelZero);
		SetBorder(Box_Util.BoxRelZero);
		SetPadding(Box_Util.BoxRelZero);
	}

	public Box ( Box_Rect r )
	{
		//Debug.Log ("BOX CONSTRUCTOR0");

		//Update parent value
		Position = new Box_Rect();
		Margin = new Box_Rel();
		Border = new Box_Rel();
		Padding = new Box_Rel();
//		Content = new Box_Rel();

		rPosition = new Box_Rect();
		rMargin = new Box_Rect();
		rBorder = new Box_Rect();
		rPadding = new Box_Rect();

		pivot = new Box_Rect();

		SetPosition(r);
		SetMargin(Box_Util.BoxRelZero);
		SetBorder(Box_Util.BoxRelZero);
		SetPadding(Box_Util.BoxRelZero);
	}

	public void SetParent(Box parent)
	{
		Parent = parent;
	}

	public void SetPivot(Vector2 piv,Box_Unit.Type type)
	{
		// pivot position set in x and y, the parent relatives are set in width and height;
		//Debug.Log(piv.x.ToString());
		pivot.x.SetValue(piv.x,type);
		pivot.y.SetValue(piv.y,type);
		
		//Update parent value
		pivot.width.SetValue( Position.width.InPixels(),Box_Unit.Type.Pixel );
		pivot.height.SetValue( Position.height.InPixels(),Box_Unit.Type.Pixel );

		//Set Parents
		pivot.x.Parent = pivot.width;
		pivot.y.Parent = pivot.height;
	}
	
	public void SetPosition ( Box_Rect Pos)
	{
		Position = Pos;

		//Update parent value
		if (Parent != null)
		{
			if (Pos.width.Parent == null)
				rPosition.width.value = Position.width.InPixels();
			else
				rPosition.width.value = Pos.width.Parent.InPixels();

			if (Pos.height.Parent == null)
				rPosition.height.value = Position.height.InPixels();
			else
				rPosition.height.value = Pos.height.Parent.InPixels();
		}
		else
		{
			//Update parent value
			rPosition.width.value = Box_Util.BoxScreen.width.InPixels();
			rPosition.height.value = Box_Util.BoxScreen.height.InPixels();
		}

		//set parent
		Position.x.Parent = rPosition.width;
		Position.y.Parent = rPosition.height;
		Position.width.Parent = rPosition.width;
		Position.height.Parent = rPosition.height;
	}

	public void SetMargin ( Box_Rel Marg )
	{
		Margin = Marg;

		//Update parent value
		if (Parent != null)
		{
			if (Marg.right.Parent == null)
				rMargin.width.value = Margin.right.InPixels() - Margin.left.InPixels();
			else
				rMargin.width.value = Marg.right.Parent.InPixels();
			
			if (Marg.bottom.Parent == null)
				rMargin.height.value = Margin.bottom.InPixels() - Margin.top.InPixels();
			else
				rMargin.height.value = Marg.bottom.Parent.InPixels();
		}
		else
		{
			//Update parent value
			rMargin.width.value = Box_Util.BoxScreen.width.InPixels();
			rMargin.height.value = Box_Util.BoxScreen.height.InPixels();
		}

		//set parent
		Margin.left.Parent = rMargin.width;
		Margin.top.Parent = rMargin.height;
		Margin.right.Parent = rMargin.width;
		Margin.bottom.Parent = rMargin.height;
	}

	public void SetBorder ( Box_Rel Bord )
	{
		Border = Bord;

		//Update parent value
		if (Parent != null)
		{
			if (Bord.right.Parent == null)
				rBorder.width.value = Border.right.InPixels() - Border.left.InPixels();
			else
				rBorder.width.value = Bord.right.Parent.InPixels();
			
			if (Bord.bottom.Parent == null)
				rBorder.height.value = Border.bottom.InPixels() - Border.top.InPixels();
			else
				rBorder.height.value = Bord.bottom.Parent.InPixels();
		}
		else
		{
			//Update parent value
			rBorder.width.value = Box_Util.BoxScreen.width.InPixels();
			rBorder.height.value = Box_Util.BoxScreen.height.InPixels();
		}

		//set parent
		Border.left.Parent = rBorder.width;
		Border.top.Parent = rBorder.height;
		Border.right.Parent = rBorder.width;
		Border.bottom.Parent = rBorder.height;
	}

	public void SetPadding ( Box_Rel Padd )
	{
		Padding = Padd;

		//Update parent value
		if (Parent != null)
		{
			if (Padd.right.Parent == null)
				rPadding.width.value = Padding.right.InPixels() - Padding.left.InPixels();
			else
				rPadding.width.value = Padd.right.Parent.InPixels();
			
			if (Padd.bottom.Parent == null)
				rPadding.height.value = Padding.bottom.InPixels() - Padding.top.InPixels();
			else
				rPadding.height.value = Padd.bottom.Parent.InPixels();
		}
		else
		{
			//Update parent value
			rPadding.width.value = Box_Util.BoxScreen.width.InPixels();
			rPadding.height.value = Box_Util.BoxScreen.height.InPixels();
		}

		//set parent
		Padding.left.Parent = rPadding.width;
		Padding.top.Parent = rPadding.height;
		Padding.right.Parent = rPadding.width;
		Padding.bottom.Parent = rPadding.height;
	}

	public Box_Rect GetPivotInPixels()
	{
		Box_Rect b = new Box_Rect();
		b.x.value = pivot.x.InPixels();
		b.y.value = pivot.y.InPixels();
		b.width = Box_Util.BoxUnitZero;
		b.height = Box_Util.BoxUnitZero;

		return b;
	}
	
	public void CalcPosition ()
	{
		//Debug.Log(pivot.x.InPixels().ToString());
		Box_Rect parentContent;
		if (Parent == null)
		{
			parentContent = Box_Util.BoxScreen;
		}
		else
		{
			parentContent = Parent.GetContentPosition();
		}

		Box_Rect b = new Box_Rect();
		Box_Rel myBoxMinimum = GetMinimumInPixels();

		b.x = new Box_Unit(Mathf.Min(parentContent.x.InPixels() + parentContent.width.InPixels() - ( myBoxMinimum.left.InPixels() + myBoxMinimum.right.InPixels() ),
		                             Mathf.Max( parentContent.x.InPixels(), (Position.x.InPixels() - pivot.x.InPixels()) + parentContent.x.InPixels() )), Box_Unit.Type.Pixel);
		b.y = new Box_Unit(Mathf.Min(parentContent.y.InPixels() + parentContent.height.InPixels() - ( myBoxMinimum.top.InPixels() + myBoxMinimum.bottom.InPixels() ), 
		                             Mathf.Max( parentContent.y.InPixels(), (Position.y.InPixels() - pivot.y.InPixels()) + parentContent.y.InPixels() )), Box_Unit.Type.Pixel);
		b.width = new Box_Unit(Mathf.Max(myBoxMinimum.left.InPixels() + myBoxMinimum.right.InPixels(), 
		                                 Mathf.Min( Position.width.InPixels(), parentContent.width.InPixels() - (parentContent.x.InPixels() - b.x.InPixels()) )), Box_Unit.Type.Pixel); 
		b.height = new Box_Unit(Mathf.Max(myBoxMinimum.top.InPixels() + myBoxMinimum.bottom.InPixels(), 
		                                  Mathf.Min( Position.height.InPixels(), parentContent.height.InPixels() - (parentContent.y.InPixels() - b.y.InPixels()) )), Box_Unit.Type.Pixel);

//		b.x.Parent = Position.width.Parent;
//		b.y.Parent = Position.height.Parent;
//		b.width.Parent = Position.width.Parent;
//		b.height.Parent = Position.height.Parent;

		Position = b;
		//return b;
	}

	public Box_Rect GetBorderPosition ()
	{
		//returning a new rect representing the position of the box from the border (forced to be in pixels)

		Box_Rect b = new Box_Rect();
		b.x.value = Position.x.InPixels() + Margin.left.InPixels() ;
		b.y.value = Position.y.InPixels() + Margin.top.InPixels() ;
		b.width.value = Position.width.InPixels() - (Margin.right.InPixels() + Margin.left.InPixels()) ;
		b.height.value = Position.height.InPixels() - (Margin.bottom.InPixels() + Margin.top.InPixels()) ;

//		b.x.Parent = Position.width.Parent;
//		b.y.Parent = Position.height.Parent;
//		b.width.Parent = Position.width.Parent;
//		b.height.Parent = Position.height.Parent;
		return b;
	}

	public Box_Rect GetContentPosition ()
	{
		//Box_Rect parentContent;
		Box_Rect b = new Box_Rect();
		Box_Rel myBoxMinimum = GetMinimumInPixels();
		b.x.value = Position.x.InPixels() + myBoxMinimum.left.InPixels();//+ Margin.left.InPixels() + Border.left.InPixels() + Padding.left.InPixels() ;
		b.y.value = Position.y.InPixels() + myBoxMinimum.right.InPixels();// + Margin.top.InPixels() + Border.top.InPixels() + Padding.top.InPixels() ;
		b.width.value = Position.width.InPixels() - (myBoxMinimum.left.InPixels() + myBoxMinimum.right.InPixels()); //- ( Margin.right.InPixels() + Border.right.InPixels() + Padding.right.InPixels() ) ;
		b.height.value =  Position.height.InPixels() - (myBoxMinimum.top.InPixels() + myBoxMinimum.bottom.InPixels()); //- ( Margin.bottom.InPixels() + Border.bottom.InPixels() + Padding.bottom.InPixels() ) ;
		
		return b;
	}

	public Box_Rel GetMinimumInPixels ()
	{
		Box_Rel b = new Box_Rel();
		b.left.value = Margin.left.InPixels() + Border.left.InPixels() + Padding.left.InPixels();
		b.top.value = Margin.top.InPixels() + Border.top.InPixels() + Padding.top.InPixels();
		b.right.value = Margin.right.InPixels() + Border.right.InPixels() + Padding.right.InPixels() ;
		b.bottom.value = Margin.bottom.InPixels() + Border.bottom.InPixels() + Padding.bottom.InPixels();
		
		return b;
	}

	public Box_Rect GetPosition ()
	{
		return Position;
	}

	public Box_Rel GetBorder ()
	{
		return Border;
	}

	public Box_Rel GetPadding ()
	{
		return Padding;
	}

	public Box_Rel GetMargin ()
	{
		return Margin;
	}


//	public Box_Rel GetContentInPixels ()
//	{
//		Content.left.value = Position.x.InPixels() + Margin.left.InPixels() + Border.left.InPixels() + Padding.left.InPixels() - pivot.x.InPixels();
//		Content.top.value = Position.y.InPixels() + Margin.top.InPixels() + Border.top.InPixels() + Padding.top.InPixels() - pivot.y.InPixels();
//		Content.right.value = ( Position.x.InPixels() + Position.width.InPixels() - pivot.x.InPixels()) - ( Margin.right.InPixels() + Border.right.InPixels() + Padding.right.InPixels() );
//		Content.bottom.value = ( Position.y.InPixels() + Position.height.InPixels() - pivot.y.InPixels()) - ( Margin.bottom.InPixels() + Border.bottom.InPixels() + Padding.bottom.InPixels() );
//		return Content;
//	}

}