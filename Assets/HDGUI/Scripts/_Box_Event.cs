﻿using UnityEngine;
using System.Collections;

public class Box_Event
{
	public const int MouseButtonLeft = 1;
	public const int MouseButtonRight = 2;
	public const int MouseButtonMiddle = 4;

	public class OnEvent
	{
	}

	public class OnMouse: OnEvent
	{
		public Vector2 position;
	}

	public class OnMouseDown:OnMouse
	{
		public int button;
		public float[] time = {0f,0f,0f};
		public float[] deltaTime = {0f,0f,0f};

		public bool GetPressed ( int MouseButton )
		{
			if (( button & MouseButton ) != 0)
				return true;
			return false;
		}
	}

	public class OnMouseUp : OnMouse
	{
		public int button;
		public float[] time = {0f,0f,0f};
		public float[] deltaTime = {0f,0f,0f};

		public bool GetPressed ( int MouseButton )
		{
			if (( button & MouseButton ) != 0)
				return true;
			return false;
		}
	}

	public class OnMouseEnter : OnMouse
	{
	}

	public class OnMouseHover : OnMouse
	{
	}

	public class OnMouseExit : OnMouse
	{
	}

	public delegate void D_OnEvent ( OnEvent e );

	public delegate void D_OnMouseDown ( OnMouseDown e );
	public delegate void D_OnMouseUp ( OnMouseUp e );
	public delegate void D_OnMouseEnter ( OnMouseEnter e );
	public delegate void D_OnMouseHover ( OnMouseHover e );
	public delegate void D_OnMouseExit ( OnMouseExit e );

	public bool mouseEnter_track = false;
	public bool mouseHover_track = false;
	public bool mouseExit_track = false;
	public float[] mouseDownTimer = {0f,0f,0f};
	public float[] mouseUpTimer = {0f,0f,0f};
}