﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Box_Page
{
	static public List<Box_Page> Pages;
	static public Box_Page FocusedPage;
	public int index;
	public string Name;

	private bool modal = false;
	private bool active = true;
	private bool locked = false;
	private bool visible = true;

	private GameObject pageRoot;

	private float rootStartDepth;
	private float rootFocusedDepth = 1000f;

	public bool isModal{ get { return modal; } }
	public bool isActive{ get { return active; } }
	public bool isLocked{ get { return locked; } }
	public bool isVisible{ get { return visible; } }
	public bool isFocused { get { return ( this == FocusedPage ); } }

	public GameObject GetPageRoot{ get { return pageRoot; } }

	public float GetRootStartDepth{ get { return rootStartDepth; } }
	public float GetRootFocusedDepth{ get { return rootFocusedDepth; } }

	private List<GameObject> elements;

	static Box_Page ()
	{
		Pages = new List<Box_Page>();
	}

	static public Box_Page Register ( string name)
	{
		if (Pages == null)
			Pages = new List<Box_Page>();
		
		Box_Page b = FindPage(name);
		if (b!= null)
		{
			//Debug.Log("Page already registered: " + name);
//			b.RegisterElement(GO);
			return b;
		}
		
		b = new Box_Page();
		b.Name = name;
		b.index = Pages.Count;
//		b.RegisterElement(GO);

		Pages.Add(b);

		return b;
	}

	public void RegisterElement(GameObject GO)
	{
		if (elements == null)
			elements = new List<GameObject>();
		if (!elements.Contains(GO))
		{
			elements.Add(GO);
			//Debug.Log("adding to elements " + elements.Count.ToString());
		}
	}

	public void UnregisterElement(GameObject GO)
	{
		if (elements == null)
			elements = new List<GameObject>();
		elements.Remove(GO);
	}

	static public Box_Page FindPage(string name)
	{
		foreach (Box_Page p in Pages)
		{
			if (p.Name == name)
			{
				return p;
			}
		}
		return null;
	}

	public void SetVisible ( bool show )
	{
		this.visible = show;
	}
	
	public void SetLock ( bool locked )
	{
		this.locked=locked;
	}

	static public void SetFocus(Box_Page Page)
	{
		if (FocusedPage == Page)
			return; //same page

		Vector3 v;
		if (FocusedPage != null) //set old focused page depth back to it's start
		{
			v = FocusedPage.GetPageRoot.transform.localPosition;
			FocusedPage.GetPageRoot.transform.localPosition = new Vector3(v.x,v.y,FocusedPage.GetRootStartDepth);
		}

		//set new focused page depth
		FocusedPage = Page;
		if (FocusedPage != null)
		{
			v = FocusedPage.GetPageRoot.transform.localPosition;
			FocusedPage.GetPageRoot.transform.localPosition = new Vector3(v.x,v.y,FocusedPage.GetRootStartDepth + FocusedPage.GetRootFocusedDepth);
		}
	}

	public void SetActive ( bool active )
	{
		if (active == this.active)
			return;

		foreach (GameObject e in elements)
		{
			if (e != pageRoot)
				e.SetActive(active);
		}

		this.active = active;
	}
	
	public void SetModal(bool modal)
	{
		this.modal = modal;
	}

	public void SetRoot(GameObject PageRoot)
	{
		pageRoot = PageRoot;
		rootStartDepth = PageRoot.transform.localPosition.z;
	}

	public void SetFocusedDepth(float depth)
	{
		this.rootFocusedDepth = depth;
	}


}
