﻿using UnityEngine;
using System.Collections;

public class Box_Rect
{
	public Box_Unit x;
	public Box_Unit y;
	public Box_Unit width;
	public Box_Unit height;
	
	public Box_Rect ()
	{
		//Debug.Log ("BOX_RECT CONSTRUCTOR0");
		this.x = new Box_Unit(0.0f);
		this.y = new Box_Unit(0.0f);
		this.width = new Box_Unit(0.0f);
		this.height = new Box_Unit(0.0f);
	}
	
	public Box_Rect ( float x, float y, float width, float height )
	{
		//Debug.Log ("BOX_RECT CONSTRUCTOR4");
		this.x = new Box_Unit(x);
		this.y = new Box_Unit(y);
		this.width = new Box_Unit(width);
		this.height = new Box_Unit(height);
	}

	public Box_Rect ( Box_Unit x, Box_Unit y, Box_Unit width, Box_Unit height )
	{
		//Debug.Log ("BOX_RECT CONSTRUCTOR4");
		this.x = x;
		this.y = y;
		this.width = width;
		this.height =height;
	}
	
	/*public static implicit operator Rect ( Box_Rect b )
	{
		return new Rect(b.x.value, b.y.value, b.width.value, b.height.value);
	}
	
	public static implicit operator Box_Rect ( Rect r )
	{
		return new Box_Rect(r.x, r.y, r.width, r.height);
	}*/
}