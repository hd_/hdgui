﻿using UnityEngine;
using System.Collections;


public class Box_Rel
{
	public Box_Unit left;
	public Box_Unit top;
	public Box_Unit right;
	public Box_Unit bottom;
	
	public Box_Rel ()
	{
		//Debug.Log ("BOX_REL CONSTRUCTOR0");
		this.left = new Box_Unit(0.0f);
		this.top = new Box_Unit(0.0f);
		this.right = new Box_Unit(0.0f);
		this.bottom = new Box_Unit(0.0f);
	}
	
	public Box_Rel ( float left, float top, float right, float bottom )
	{
		//Debug.Log ("BOX_REL CONSTRUCTOR0");
		this.left = new Box_Unit(left);
		this.top = new Box_Unit(top);
		this.right = new Box_Unit(right);
		this.bottom = new Box_Unit(bottom);
	}

	public Box_Rel ( Box_Unit left, Box_Unit top, Box_Unit right, Box_Unit bottom )
	{
		//Debug.Log ("BOX_REL CONSTRUCTOR0");
		this.left = left;
		this.top = top;
		this.right = right;
		this.bottom = bottom;
	}
	
	/*public static explicit operator Rect ( Box_Rel b )
	{
		return new Rect(b.left.value, b.top.value, b.right.value, b.bottom.value);
	}
	
	public static explicit operator Box_Rel ( Rect r )
	{
		return new Box_Rel(r.x, r.y, r.width, r.height);
	}*/

	/*public static implicit operator RectOffset ( Box_Rel b )
	{
		return new RectOffset(b.left.value, b.top.value, b.right.value, b.bottom.value);
	}
	
	public static implicit operator Box_Rel ( RectOffset r )
	{
		return new Box_Rel(r.left, r.top, r.right, r.bottom);
	}*/
}