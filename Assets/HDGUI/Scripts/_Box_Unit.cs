﻿using UnityEngine;
using System.Collections;

public class Box_Unit
{
//	public struct DPI
//	{
//		public float dpi;
//	}
//
//	public struct Relative
//	{
//		public Box_Unit value;
//	}

	private static float _dpi;
	public Box_Unit Parent;

	public static float DPI
	{
		get{ return _dpi;}
		set{ _dpi = value;}
	}

	public enum Type
	{
		Pixel,
		Percent,
		Point
	}
	;
	private Type _type = Type.Pixel;
	private float _pixel = 0;
	private float _percent = 0;
	private float _point = 0;
	
	private Type type
	{
		get{ return _type;}
		set{ _type = value;}
	}
	private float pixel
	{
		get{ return _pixel;}
		set
		{
			//Debug.Log (value.ToString ());
			_pixel = value;
		}
	}
	 
	private float percent
	{
		get{ return _percent;}
		set{ _percent = value;}
	}

	private float point
	{
		get{ return _point;}
		set{ _point = value;}
	}

	public static void CalcDPI ()
	{
		DPI = Screen.dpi;
		if (DPI == 0.0f)
		{
			DPI = 96.0f;
		}
	}
	static Box_Unit ()
	{
		CalcDPI();
	}

	public Box_Unit ()
	{
		//Debug.Log ("BOX_UNI CONSTRUCTOR0");
		//CalcDPI();
		type = Type.Pixel;
	}

	public Box_Unit ( float f )
	{
		//Debug.Log ("BOX_UNIT CONSTRUCTOR1");
		//CalcDPI();
		pixel = f;
		type = Type.Pixel;
	}
	
	public Box_Unit ( float value, Type type)
	{
		//CalcDPI();
		this.type = type;
		switch ( type )
		{
			case Type.Pixel:
				pixel = value;
				break;
			case Type.Percent:
				percent = value;
				break;
			case Type.Point:
				point = value;
				break;
			default:
				pixel = value;
				this.type = Type.Pixel;
				break;
		}
	}


	public float value
	{
		get
		{
			switch ( type )
			{
				case Type.Pixel:
					return pixel;
				case Type.Percent:
					return percent;
				case Type.Point:
					return point;
				default:
					return pixel;
			}
		}
		set
		{
			switch ( type )
			{
				case Type.Pixel:
					pixel = value;
					break;
				case Type.Percent:
					percent = value;
					break;
				case Type.Point:
					point = value;
					break;
				default:
					pixel = value;
					break;
			}
		}
	}
	
	public float Pixel
	{
		get { return pixel;}
		set
		{
			type = Type.Pixel;
			pixel = value;
		}
	}
	public float Point
	{
		get { return point;}
		set
		{
			type = Type.Point;
			point = value;
		}
	}
	public float Percent
	{
		get { return percent;}
		set
		{
			type = Type.Percent;
			percent = value;
		}
	}

	public void SetValue(float value,Type type)
	{
		this.type=type;
		switch ( type )
		{
			case Type.Pixel:
				pixel = value;
				break;
			case Type.Percent:
				percent = value;
				break;
			case Type.Point:
				point = value;
				break;
			default:
				pixel = value;
				break;
		}
	}
	
	public float InPixels ()
	{
		switch ( type )
		{
			case Type.Pixel:
				return pixel;
			case Type.Percent:
				if (Parent == null)
				{
					Debug.Log("Error: InPixels() called as Percent with no Parent !");
						return 1.0f;
				}
				else{
					return percent * Parent.InPixels();
				}
			case Type.Point:
				return point * 72.0f / DPI;
			default:
				Debug.Log("Warning: InPixels() was called with unknown type");
				return pixel;
		}
	}
}