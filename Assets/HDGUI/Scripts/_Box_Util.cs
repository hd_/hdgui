﻿using UnityEngine;
using System.Collections;

public static class Box_Util
{
	private static Box_Unit _BoxUnitZero;
	private static Box_Rect _BoxRectZero;
	private static Box_Rel _BoxRelZero;
	private static Box_Rect _BoxScreen;
	
	public static Box_Unit BoxUnitZero
	{
		get{ return _BoxUnitZero;}
	}
	public static Box_Rect BoxRectZero
	{
		get{ return _BoxRectZero;}
	}
	public static Box_Rel BoxRelZero
	{
		get{ return _BoxRelZero;}
	}
	
	public static Box_Rect BoxScreen
	{
		get{ return _BoxScreen;}
	}

	public static void BoxScreenUpdate ()
	{
		_BoxScreen.width.value = Screen.width;
		_BoxScreen.height.value = Screen.height;
	}
	
	static Box_Util ()
	{
		_BoxUnitZero = new Box_Unit();
		_BoxRectZero = new Box_Rect();
		_BoxRelZero = new Box_Rel();
		
		_BoxScreen = new Box_Rect();
		BoxScreenUpdate();
	}
}